package tfc.biblioteca.rest.image;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.image.ImageDTO;
import tfc.biblioteca.domain.image.ImageReadDTO;

import java.util.List;

@Mapper
public interface ImageReadDTOMapper {
    public ImageReadDTO toDTO(ImageReadTO dto);
    public ImageReadTO toTO(ImageReadDTO  dto);
    public List<ImageReadDTO > toDTOList(List<ImageReadTO> list);
    public List<ImageReadTO> toTOList(List<ImageReadDTO> list);
}
