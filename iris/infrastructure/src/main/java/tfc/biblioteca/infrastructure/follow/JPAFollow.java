package tfc.biblioteca.infrastructure.follow;

import org.springframework.data.repository.CrudRepository;

public interface JPAFollow extends CrudRepository<Follow, Long> {

    Follow findByFollowerAndFollowing(Long follower, Long following);

}
