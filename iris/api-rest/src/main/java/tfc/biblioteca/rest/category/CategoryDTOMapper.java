package tfc.biblioteca.rest.category;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.category.CategoryDTO;

import java.util.List;

@Mapper
public interface CategoryDTOMapper {

    public CategoryDTO toCategoryDTO(CategoryTO dto);
    public CategoryTO toTO(CategoryDTO dto);

    public List<CategoryDTO> toDTOList(List<CategoryTO> list);
    public List<CategoryTO> toTOList(List<CategoryDTO> list);


}
