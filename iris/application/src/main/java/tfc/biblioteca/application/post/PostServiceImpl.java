package tfc.biblioteca.application.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.domain.post.PostReadDTO;
import tfc.biblioteca.domain.post.PostRepository;
import tfc.biblioteca.domain.post.PostService;
import tfc.biblioteca.domain.shelf.ShelfDTO;
import tfc.biblioteca.domain.shelf.ShelfRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    PostRepository repository;

    @Autowired
    ShelfRepository shelfRepository;

    @Override
    public List<PostReadDTO> getAll() {
        List<PostReadDTO> list = repository.getAll();
        return repository.getAll();
    }

    @Override
    public PostReadDTO getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public List<PostReadDTO> getByUserId(Long id) {
        return repository.getByUserId(id);
    }

    @Override
    public PostDTO savePost(PostDTO post) {
        List<ShelfDTO> shelves =  shelfRepository.getAllByUserId(post.getUserId());

        PostDTO savedPost = repository.savePost(post);

        return savedPost;
    }
}
