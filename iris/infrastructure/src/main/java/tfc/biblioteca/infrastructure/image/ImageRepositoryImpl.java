package tfc.biblioteca.infrastructure.image;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tfc.biblioteca.domain.image.ImageDTO;
import tfc.biblioteca.domain.image.ImageRepository;

import java.util.List;

@Repository
public class ImageRepositoryImpl implements ImageRepository {

    @Autowired
    ImageMapper mapper;

    @Autowired
    JPAImage jpaImage;
    @Override
    public ImageDTO save(ImageDTO dtos) {
        Image images = mapper.toEntity(dtos);

        return mapper.toDto(jpaImage.save(images)) ;
    }
}
