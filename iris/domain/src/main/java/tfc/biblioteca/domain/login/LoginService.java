package tfc.biblioteca.domain.login;

import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.user.UserDTO;

@Service
public interface LoginService {

    public boolean authorizeLogin(LoginDTO userLogin);
    public UserDTO getUserInfo(LoginDTO loginDTO);
}
