package tfc.biblioteca.rest.file;

import jakarta.annotation.Nullable;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUpload;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tfc.biblioteca.domain.file.FileService;
import tfc.biblioteca.domain.file.UploadFileDTO;

import java.io.*;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@Controller
@AllArgsConstructor
@RequestMapping("/file")
@ComponentScan(basePackages = "tfc.biblioteca.rest")
public class FileController {

    @Autowired
    FileService service;

    @Autowired
    FileTOMapper mapper;

    @Autowired
    UploadFileTOMapper uploadMapper;

    @GetMapping(value = "/all")
    ResponseEntity<List<FileTO>> getAllScores() {
        return new ResponseEntity<>(mapper.toTOList(service.getAll()), HttpStatus.OK);

    }

    /*@GetMapping(value = "/{bookId}")
    ResponseEntity<FileTO> getBookById(@PathVariable Long bookId){
        return new ResponseEntity<>(mapper.toTO(service.getById(bookId)), HttpStatus.OK);
    }*/

    @GetMapping(value= "/name/{bookName}")
    ResponseEntity<List<FileTO>> getBookByName(@PathVariable String bookName){
        return new ResponseEntity<>(mapper.toTOList(service.getByName(bookName)), HttpStatus.OK);
    }

    @GetMapping(value = "/{postId}")
    @ResponseBody ResponseEntity getFileByPostId(HttpServletResponse response, @PathVariable Long postId) throws IOException {
        File file =  service.getFileByPostId(postId);
        InputStream inputStream = new FileInputStream(file); // load the file
        IOUtils.copy(inputStream, response.getOutputStream());
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition", " filename=" + file.getName() + ".pdf");
        response.flushBuffer();

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/{postId}")
    ResponseEntity<String> postBookFile(@RequestParam @Nullable MultipartFile file, @PathVariable Long postId){
        UploadFileTO to = new UploadFileTO(file, postId );
        service.saveBook(uploadMapper.toDTO(to));
        return new ResponseEntity<>("Todo genial", HttpStatus.I_AM_A_TEAPOT);
    }
}
