package tfc.biblioteca.rest.post;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.domain.post.PostReadDTO;

import java.util.List;

@Mapper
public interface PostReadTOMapper {

    public PostReadDTO toDTO(PostReadTO dto);
    public PostReadTO toTO(PostReadDTO dto);

    public List<PostReadDTO > toDTOList(List<PostReadTO> list);
    public List<PostReadTO> toTOList(List<PostReadDTO > list);
}
