package tfc.biblioteca.domain.file;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;
import tfc.biblioteca.domain.category.CategoryDTO;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileDTO {


    private MultipartFile file;
    private Long postId;
}
