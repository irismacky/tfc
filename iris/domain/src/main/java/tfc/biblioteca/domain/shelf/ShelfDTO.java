package tfc.biblioteca.domain.shelf;

import lombok.Data;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.domain.post.PostReadDTO;

import java.util.List;
@Data
public class ShelfDTO {


    private Long id;


    private String name;


    private String color;
    private Long userId;


    private List<PostReadDTO> posts;
}
