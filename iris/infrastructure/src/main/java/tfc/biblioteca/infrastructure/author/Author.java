package tfc.biblioteca.infrastructure.author;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "t_author")
public class Author {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "author_id")
    private Long id;

    @Column(name = "author_name")
    private String name;

    @Column(name = "author_bio")
    private String bio;

   /* @OneToMany( mappedBy = "author")
    private List<Book> books;*/

}
