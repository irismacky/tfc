package tfc.biblioteca.domain.file;

import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
public interface FileService {
    public List<FileDTO> getAll();
    public FileDTO getById(Long id);

    public List<FileDTO> getByName(String name);

    public File getFileByPostId(Long id);

    public List<FileDTO> getBooksByCategory(Long id);

    public UploadFileDTO saveBook(UploadFileDTO dto);
}
