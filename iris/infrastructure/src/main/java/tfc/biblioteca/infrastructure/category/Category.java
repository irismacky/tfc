package tfc.biblioteca.infrastructure.category;

import jakarta.persistence.*;
import lombok.*;
import tfc.biblioteca.infrastructure.post.Post;

import java.util.List;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_category")
public class Category {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long id;

    @Column(name = "category_name")
    private String name;

    @ManyToMany
    @JoinTable(
            name = "t_post_category",
            joinColumns = @JoinColumn(name = "category_id"),
            inverseJoinColumns = @JoinColumn(name = "post_id"))
    private List<Post> posts;

   /* @ManyToMany
    @JoinTable(
            name = "t_book_category",
            joinColumns = @JoinColumn(name = "category_id"),
            inverseJoinColumns = @JoinColumn(name = "book_id"))
    private List<Book> books;*/
}
