package tfc.biblioteca.infrastructure.image;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.image.ImageDTO;
import tfc.biblioteca.domain.image.ImageReadDTO;

import java.util.List;

@Mapper
public interface ImageReadMapper {
    Image toEntity(ImageReadDTO dto);
    ImageReadDTO toDto(Image entity);
    List<Image> toEntityList(List<ImageReadDTO> list);
    List<ImageReadDTO> toDtoList(List<Image> list);
}
