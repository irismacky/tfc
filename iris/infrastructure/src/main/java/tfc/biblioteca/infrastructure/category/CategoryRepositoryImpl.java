package tfc.biblioteca.infrastructure.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tfc.biblioteca.domain.category.CategoryDTO;
import tfc.biblioteca.domain.category.CategoryRepository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    @Autowired
    JPACategory jpa;

    @Autowired
    CategoryMapper mapper;
    @Override
    public List<CategoryDTO> getAll() {
        List<Category> categories = jpa.findAll();
        return mapper.toDtoList(jpa.findAll());
    }

    @Override
    public CategoryDTO findById(Long id) {
        return mapper.toDto(jpa.findById(id).get());
    }
}
