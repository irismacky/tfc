package tfc.biblioteca.domain.file;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository {

    public List<FileDTO> getAll();

    public FileDTO getById(Long id);
    public FileDTO getByPostId(Long id);

    public List<FileDTO> getByName(String name);

    public List<FileDTO> getListByCategoryId(Long id);

    public UploadFileDTO saveBook(UploadFileDTO dto, String path);
}
