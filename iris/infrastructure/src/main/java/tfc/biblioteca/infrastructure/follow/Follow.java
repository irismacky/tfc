package tfc.biblioteca.infrastructure.follow;

import jakarta.persistence.*;
import lombok.Data;
import tfc.biblioteca.infrastructure.user.User;

@Entity
@Data
@Table(name="t_follow")
public class Follow {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "follow_id")
    private Long id;

    @Column(name="following_id")
    private Long following;
    @Column(name = "follower_id")
    private Long follower;


}
