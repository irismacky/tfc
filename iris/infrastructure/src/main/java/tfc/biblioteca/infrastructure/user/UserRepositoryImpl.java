package tfc.biblioteca.infrastructure.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tfc.biblioteca.domain.login.LoginDTO;
import tfc.biblioteca.domain.user.UserDTO;
import tfc.biblioteca.domain.user.UserRepository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    JPAUser jpaUser;

    @Autowired
    UserMapper mapper;
    @Override
    public LoginDTO getLoginCredentials(String username) {
        Optional<User> optionalUser = jpaUser.findByUsername(username);
        User user = new User();
        try {
            user = optionalUser.get();
        }catch (Exception e){
            System.out.printf(e.getMessage());
        }

        LoginDTO login = new LoginDTO(user.getUsername(), user.getPassword());

        return login;
    }

    @Override
    public UserDTO getUserByUsername(String username) {
        UserDTO userReturn = new UserDTO();
        try {
            userReturn = mapper.toDTO(jpaUser.findByUsername(username).get());
        } catch (Exception e){
            userReturn = null;
        }
        return userReturn ;
    }

    @Override
    public UserDTO save(UserDTO dto) {
        User user = mapper.toEntity(dto);
        User userSaved = jpaUser.save(user);
        return mapper.toDTO(userSaved);
    }

    @Override
    public UserDTO update(UserDTO dto) {
        return mapper.toDTO(jpaUser.save(mapper.toEntity(dto)));
    }
}
