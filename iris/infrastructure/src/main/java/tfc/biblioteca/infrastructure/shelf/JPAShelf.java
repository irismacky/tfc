package tfc.biblioteca.infrastructure.shelf;

import org.springframework.data.jpa.repository.JpaRepository;
import tfc.biblioteca.infrastructure.user.User;

import java.util.List;

public interface JPAShelf extends JpaRepository<Shelf, Long> {

    List<Shelf> findAllByUser(User user);
}
