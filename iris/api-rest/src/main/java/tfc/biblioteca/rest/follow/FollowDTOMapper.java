package tfc.biblioteca.rest.follow;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.follow.FollowDTO;
import tfc.biblioteca.domain.user.UserDTO;
import tfc.biblioteca.rest.user.UserTO;

@Mapper
public interface FollowDTOMapper {

    FollowTO toTO(FollowDTO dto);
    FollowDTO toDTO(FollowTO to);
}
