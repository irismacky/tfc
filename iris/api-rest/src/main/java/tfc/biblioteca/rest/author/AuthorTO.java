package tfc.biblioteca.rest.author;

import lombok.Data;

@Data
public class AuthorTO {

    private Long id;
    private String name;
    private String bio;
}
