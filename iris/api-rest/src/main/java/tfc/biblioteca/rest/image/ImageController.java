package tfc.biblioteca.rest.image;

import jakarta.annotation.Nullable;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tfc.biblioteca.domain.exceptions.UsernameTakenException;
import tfc.biblioteca.domain.image.ImageService;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.domain.user.UserService;
import tfc.biblioteca.rest.post.PostTO;
import tfc.biblioteca.rest.user.UserDTOMapper;
import tfc.biblioteca.rest.user.UserTO;

import java.io.IOException;
import java.util.List;
@CrossOrigin(origins = "*")
@Controller
@ComponentScan(basePackages = "tfc.biblioteca.rest")
@RequestMapping("/img")
public class ImageController {

    @Autowired
    ImageService service;

    @Autowired
    ImageDTOMapper mapper;

    @Autowired
    UserService userService;

    @Autowired
    UserDTOMapper userMapper;


    @PostMapping(value = "/post/{postId}")
    @ResponseBody ImageTO postImg(@RequestParam @Nullable MultipartFile imageFile, @PathVariable Long postId) throws IOException {
        ImageTO image = new ImageTO();
        PostTO post = new PostTO();
        post.setId(postId);
        image.setPost(post);
        image.setImage(imageFile.getBytes());
        return mapper.toTO(service.save(mapper.toDTO(image)));
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/user/{username}")
    @ResponseBody UserTO postUserImg(@RequestParam @Nullable MultipartFile imageFile, @PathVariable String username) throws IOException, UsernameTakenException {
        UserTO userReturn = new UserTO();
        if (imageFile == null){
            userReturn = userMapper.toTO(userService.getByUsername(username));
        }
        else {
            userMapper.toTO(userService.addUserImg(imageFile.getBytes(), username));
        }


        return userReturn;
    }


}
