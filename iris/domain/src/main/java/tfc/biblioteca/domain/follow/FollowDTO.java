package tfc.biblioteca.domain.follow;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FollowDTO {

    private Long id;


    private Long following;

    private Long follower;
}
