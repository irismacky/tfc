package tfc.biblioteca.rest.shelf;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.category.CategoryDTO;
import tfc.biblioteca.domain.shelf.ShelfDTO;
import tfc.biblioteca.rest.category.CategoryTO;

import java.util.List;

@Mapper
public interface ShelfDTOMapper {

    public ShelfDTO toDTO(ShelfTO dto);
    public ShelfTO toTO(ShelfDTO dto);

    public List<ShelfDTO> toDTOList(List<ShelfTO> list);
    public List<ShelfTO> toTOList(List<ShelfDTO> list);
}
