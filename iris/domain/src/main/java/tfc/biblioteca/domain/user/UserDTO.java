package tfc.biblioteca.domain.user;


import lombok.*;
import tfc.biblioteca.domain.shelf.ShelfDTO;

import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {



    private Long id;


    private String username;


    private String password;


    private byte[] img;


    private String description;

    private String joinDate;

    private List<UserBasicDTO> followers;
    private List<UserBasicDTO> following;






}
