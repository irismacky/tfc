package tfc.biblioteca.domain.follow;

import org.springframework.stereotype.Repository;
import tfc.biblioteca.domain.user.UserDTO;

@Repository
public interface FollowRepository {

    UserDTO save(FollowDTO follow);
    FollowDTO delete(FollowDTO followDTO);
}
