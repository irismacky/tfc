package tfc.biblioteca.infrastructure.image;

import jakarta.persistence.*;
import lombok.*;
import tfc.biblioteca.infrastructure.post.Post;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_image")
public class Image {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "image_id")
    private Long id;

    @Column(name = "image_file")
    @Lob
    private byte[] image;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;
}
