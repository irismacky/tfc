package tfc.biblioteca.application.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.login.LoginDTO;
import tfc.biblioteca.domain.login.LoginService;
import tfc.biblioteca.domain.user.UserDTO;
import tfc.biblioteca.domain.user.UserRepository;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserRepository repository;


    @Override
    public boolean authorizeLogin(LoginDTO userLogin) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        LoginDTO loginDataBase = repository.getLoginCredentials(userLogin.getUsername());
        return encoder.matches(userLogin.getPassword(), loginDataBase.getPassword());
    }

    @Override
    public UserDTO getUserInfo(LoginDTO loginDTO) {
        UserDTO user = new UserDTO();
        if (authorizeLogin(loginDTO)){
            user = repository.getUserByUsername(loginDTO.getUsername());
        }else{
            System.out.printf("USUARIO O CONTRASEÑA INCORRECTOS");
        }
        return user;
    }
}
