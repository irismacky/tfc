package tfc.biblioteca.domain.exceptions;

public class UsernameTakenException extends Exception{

    public UsernameTakenException() {
        super("Username Taken");
    }
}
