package tfc.biblioteca.domain.post;

import lombok.*;
import tfc.biblioteca.domain.category.CategoryDTO;
import tfc.biblioteca.domain.image.ImageDTO;
import tfc.biblioteca.domain.image.ImageReadDTO;
import tfc.biblioteca.domain.user.UserDTO;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostReadDTO {

    private Long id;

    private String title;

    private String summary;

    private String content;

    private Date date;
    private UserDTO user;

    private List<ImageReadDTO> images;

    private List<CategoryDTO> categories;
}
