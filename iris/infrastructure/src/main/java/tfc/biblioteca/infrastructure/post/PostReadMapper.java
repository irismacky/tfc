package tfc.biblioteca.infrastructure.post;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.domain.post.PostReadDTO;

import java.util.List;

@Mapper
public interface PostReadMapper {

    Post toEntity(PostReadDTO dto);


    PostReadDTO toDto(Post entity);

    List<Post> toEntityList(List<PostReadDTO> list);
    List<PostReadDTO> toDtoList(List<Post> list);
}
