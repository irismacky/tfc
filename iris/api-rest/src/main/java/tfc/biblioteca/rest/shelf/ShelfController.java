package tfc.biblioteca.rest.shelf;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.domain.shelf.ShelfDTO;
import tfc.biblioteca.domain.shelf.ShelfService;
import tfc.biblioteca.rest.category.CategoryTO;
import tfc.biblioteca.rest.post.PostTO;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@Controller
@AllArgsConstructor
@RequestMapping("/shelf")
public class ShelfController {

    @Autowired
    ShelfService service;

    @Autowired
    ShelfDTOMapper mapper;

    @GetMapping(value = "/user/{userId}")
    ResponseEntity<List<ShelfTO>> getAllShelvesByUserId(@PathVariable Long userId) {

        return new ResponseEntity<>(mapper.toTOList(service.getAllByUserId(userId)), HttpStatus.OK);

    }

    @PostMapping
    ResponseEntity<ShelfTO> postShelf(@RequestBody ShelfTO shelf){
        ShelfDTO savedShelf = service.save(mapper.toDTO(shelf));
        return new ResponseEntity<>(mapper.toTO(savedShelf), HttpStatus.OK);
    }

    @PostMapping("/{shelfId}/post/{postId}")
    ResponseEntity<ShelfTO> savePostToSelf(@PathVariable Long shelfId, @PathVariable Long postId){
        ShelfDTO savedShelf = service.addPostToShelf(shelfId, postId);
        return new ResponseEntity<>(mapper.toTO(savedShelf), HttpStatus.OK);
    }
}
