package tfc.biblioteca.rest.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tfc.biblioteca.domain.user.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService service;

    @Autowired
    UserDTOMapper mapper;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/{username}")
    ResponseEntity<UserTO> getUserByName(@PathVariable String username){
        return new ResponseEntity<>(mapper.toTO(service.getByUsername(username)), HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PatchMapping(value = "/{username}")
    ResponseEntity<UserTO> updateUser(@PathVariable String username, @RequestBody UserTO to){
        return new ResponseEntity<>(mapper.toTO(service.patchUpdate(mapper.toDTO(to), username)), HttpStatus.OK);
    }



}
