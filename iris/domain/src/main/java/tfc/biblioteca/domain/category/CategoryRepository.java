package tfc.biblioteca.domain.category;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    List<CategoryDTO> getAll();
    CategoryDTO findById(Long id);
}
