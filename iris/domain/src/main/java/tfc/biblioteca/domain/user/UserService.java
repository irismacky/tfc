package tfc.biblioteca.domain.user;

import org.mapstruct.control.MappingControl;
import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.exceptions.UsernameTakenException;

@Service
public interface UserService {

    UserDTO save(UserDTO dto) throws UsernameTakenException;

    UserDTO getByUsername(String username);

    UserDTO patchUpdate(UserDTO dto, String username);

    UserDTO addUserImg(byte[] img, String username);

}
