package tfc.biblioteca.infrastructure.image;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.image.ImageDTO;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.infrastructure.post.Post;

import java.util.List;

@Mapper
public interface ImageMapper {

    Image toEntity(ImageDTO dto);


    ImageDTO toDto(Image entity);

    List<Image> toEntityList(List<ImageDTO> list);
    List<ImageDTO> toDtoList(List<Image> list);
}
