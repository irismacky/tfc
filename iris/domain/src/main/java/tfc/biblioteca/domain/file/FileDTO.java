package tfc.biblioteca.domain.file;

import lombok.Data;
import tfc.biblioteca.domain.author.AuthorDTO;
import tfc.biblioteca.domain.category.CategoryDTO;

import java.util.List;

@Data
public class FileDTO {

    private Long id;



    private String filePath;



}
