package tfc.biblioteca.application.file;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tfc.biblioteca.domain.file.StorageService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Service
public class StorageServiceImpl implements StorageService {

    private final Path root = Paths.get("files");
    @Override
    public void init() {
        try {
            Files.createDirectories(root);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder");
        }

    }

    @Override
    public String saveFile(MultipartFile file) {
        Path path = root.resolve(Objects.requireNonNull(file.getOriginalFilename()));
        boolean keepTrying = true;
        int index = 1;
        while(keepTrying){
            try {
                Files.copy(file.getInputStream(), path);
                keepTrying = false;
            } catch (Exception e) {
                if (e instanceof FileAlreadyExistsException) {
                    path = root.resolve(index + file.getOriginalFilename());
                    index++;
                }
                if (e instanceof  NullPointerException){
                    System.out.println("Can't find file");
                }
            }
        }
        return path.toString() ;
    }

    @Override
    public File getFileFromPath(String path) throws IOException {

        File file = new File(path);
        FileInputStream input = new FileInputStream(file);
        MultipartFile result = new MockMultipartFile("file", file.getName(), "application/pdf", input);
        return file;
    }


}
