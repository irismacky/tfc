package tfc.biblioteca.rest.image;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.image.ImageDTO;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.rest.post.PostTO;

import java.util.List;

@Mapper
public interface ImageDTOMapper {

    public ImageDTO toDTO(ImageTO dto);
    public ImageTO toTO(ImageDTO  dto);

    public List<ImageDTO > toDTOList(List<ImageTO> list);
    public List<ImageTO> toTOList(List<ImageDTO> list);
}
