package tfc.biblioteca.infrastructure.image;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPAImage extends JpaRepository<Image, Long> {
}
