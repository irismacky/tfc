package tfc.biblioteca.rest.shelf;

import lombok.Data;
import tfc.biblioteca.rest.post.PostReadTO;
import tfc.biblioteca.rest.post.PostTO;

import java.util.List;

@Data
public class ShelfTO {

    private Long id;


    private String name;


    private String color;
    private Long userId;


    private List<PostReadTO> posts;
}
