package tfc.biblioteca.infrastructure.user;


import jakarta.persistence.*;
import lombok.Data;
import tfc.biblioteca.infrastructure.post.Post;
import tfc.biblioteca.infrastructure.shelf.Shelf;

import java.util.List;

@Entity
@Data
@Table(name = "t_user")
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(name="user_name")
    private String username;

    @Column(name="user_password")
    private String password;

    @Column(name="user_email")
    private String email;

    @Column(name="user_img")
    @Lob
    private byte[] img;

    @Column(name="user_description")
    private String description;

    @Column(name="user_join_date")
    private String joinDate;

    @OneToMany(mappedBy = "user")
    private List<Shelf> shelves;

    @OneToMany(mappedBy = "user")
    private List<Post> posts;

    @ManyToMany
    @JoinTable(
            name = "t_follow",
            joinColumns = @JoinColumn(name = "following_id"),
            inverseJoinColumns = @JoinColumn(name = "follower_id"))
    List<User> followers;

    @ManyToMany
    @JoinTable(
            name = "t_follow",
            joinColumns = @JoinColumn(name = "follower_id"),
            inverseJoinColumns = @JoinColumn(name = "following_id"))
    List<User> following;



}
