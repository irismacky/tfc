package tfc.biblioteca.infrastructure.post;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tfc.biblioteca.infrastructure.author.Author;
import tfc.biblioteca.infrastructure.category.Category;
import tfc.biblioteca.infrastructure.image.Image;
import tfc.biblioteca.infrastructure.shelf.Shelf;
import tfc.biblioteca.infrastructure.user.User;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_post")
public class Post  {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "post_id")
    private Long id;

    @Column(name= "post_title")
    private String title;

    @Column(name="post_summary")
    private String summary;

    @Column(name="post_content")
    private String content;

    @Column(name="post_date")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "post")
    private List<Image> images;


    @ManyToMany
    @JoinTable(
            name = "t_post_category",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    List<Category> categories;

    @ManyToMany
    @JoinTable(
            name = "t_post_shelf",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "shelf_id"))
    List<Shelf> shelves;
}
