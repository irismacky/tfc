package tfc.biblioteca.application.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.file.FileDTO;
import tfc.biblioteca.domain.file.FileRepository;
import tfc.biblioteca.domain.file.FileService;
import tfc.biblioteca.domain.file.UploadFileDTO;


import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {

    @Autowired
    FileRepository fileRepository;

    @Autowired
    StorageServiceImpl service;


    @Override
    public List<FileDTO> getAll() {
        List<FileDTO> books = fileRepository.getAll();
        return fileRepository.getAll() ;
    }

    @Override
    public FileDTO getById(Long id) {
        return fileRepository.getById(id);
    }

    @Override
    public List<FileDTO> getByName(String name) {
        return fileRepository.getByName(name);
    }

    @Override
    public File getFileByPostId(Long id) {
    FileDTO fileDTO = fileRepository.getByPostId(id);
        String path = fileDTO.getFilePath();

        try {
            return  service.getFileFromPath(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<FileDTO> getBooksByCategory(Long id) {
        return fileRepository.getListByCategoryId(id);
    }

    @Override
    public UploadFileDTO saveBook(UploadFileDTO dto) {
        String path = service.saveFile(dto.getFile());
        return fileRepository.saveBook(dto, path);
    }
}
