package tfc.biblioteca.rest.post;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.category.CategoryDTO;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.rest.category.CategoryTO;

import java.util.List;

@Mapper
public interface PostTOMapper {

    public PostDTO toDTO(PostTO dto);
    public PostTO toTO(PostDTO  dto);

    public List<PostDTO > toDTOList(List<PostTO> list);
    public List<PostTO> toTOList(List<PostDTO > list);
}
