package tfc.biblioteca.rest.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.domain.post.PostService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/post")
public class PostController {

    @Autowired
    PostService service;

    @Autowired
    PostTOMapper mapper;

    @Autowired
    PostReadTOMapper readTOMapper;

    @GetMapping(value = "/all")
    ResponseEntity<List<PostReadTO>> getAllPosts() {
        List <PostReadTO> list = readTOMapper.toTOList(service.getAll());
        return new ResponseEntity<>(list, HttpStatus.OK);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/{postId}")
    ResponseEntity<PostReadTO> getPostById(@PathVariable Long postId) {
        PostReadTO post = readTOMapper.toTO(service.getById(postId));
        return new ResponseEntity<PostReadTO>(post, HttpStatus.OK);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/user/{userId}")
    ResponseEntity<List<PostReadTO>> getPostByUserId(@PathVariable Long userId) {
        List <PostReadTO> list = readTOMapper.toTOList(service.getByUserId(userId));
        return new ResponseEntity<>(list, HttpStatus.OK);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<PostTO> postBookFile(@RequestBody PostTO post){
        PostDTO postDTO = service.savePost(mapper.toDTO(post));
        return new ResponseEntity<>(mapper.toTO(postDTO), HttpStatus.OK);
    }
}
