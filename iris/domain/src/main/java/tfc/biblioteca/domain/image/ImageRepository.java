package tfc.biblioteca.domain.image;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository {

    ImageDTO save(ImageDTO dtos);
}
