package tfc.biblioteca.rest.user;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.user.UserDTO;

@Mapper
public interface UserDTOMapper {

    UserTO toTO(UserDTO dto);
    UserDTO toDTO(UserTO to);
}
