package tfc.biblioteca.infrastructure.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.domain.post.PostReadDTO;
import tfc.biblioteca.domain.post.PostRepository;
import tfc.biblioteca.domain.shelf.ShelfDTO;
import tfc.biblioteca.infrastructure.image.Image;
import tfc.biblioteca.infrastructure.shelf.JPAShelf;
import tfc.biblioteca.infrastructure.shelf.Shelf;
import tfc.biblioteca.infrastructure.user.JPAUser;
import tfc.biblioteca.infrastructure.user.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class PostRepositoryImpl implements PostRepository {
    @Autowired
    JPAPost jpaPost;
    @Autowired
    JPAUser jpaUser;

    @Autowired
    JPAShelf jpaShelf;

    @Autowired
    PostMapper mapper;

    @Autowired
    PostReadMapper readMapper;


    @Override
    public List<PostReadDTO> getAll() {
        List<Post> list = jpaPost.findAll();
        return readMapper.toDtoList(jpaPost.findAll());
    }

    @Override
    public PostReadDTO getById(Long id) {
        Post post = jpaPost.findById(id).get();

        return readMapper.toDto(post);
    }

    @Override
    public List<PostReadDTO> getByUserId(Long id) {
        User user = jpaUser.findById(id).get();
        return readMapper.toDtoList(user.getPosts());
    }

    @Override
    public PostDTO savePost(PostDTO post) {
        Post postEntity = mapper.toEntity(post);
        postEntity.setUser(jpaUser.findById(post.getUserId()).get());
        postEntity.setDate(new Date());
        List<Shelf> allShelves = jpaShelf.findAllByUser(postEntity.getUser());
        List<Shelf> filteredShelves = new ArrayList<>();

        Long shelfId =  -1L;
        for (Shelf shelf: allShelves){
            if (shelf.getName().equals("Subidos")){
                shelfId = shelf.getId();
            }
        }

        if (shelfId == -1L){
            Shelf newShelf = new Shelf();
            newShelf.setName("Subidos");
            newShelf.setUser(jpaUser.findById(post.getUserId()).get());
            allShelves.add(jpaShelf.save(newShelf));
        }

        if (post.getShelfIds() != null){
            allShelves.forEach((shelf -> {
                if (post.getShelfIds().contains(shelf.getId()) || shelf.getName().equals("Subidos")){
                    filteredShelves.add(shelf);
                }
            }));
        }

        postEntity.setShelves(filteredShelves);



        Post saved = jpaPost.save(postEntity);

        return mapper.toDto(saved);
    }
}
