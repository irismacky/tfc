package tfc.biblioteca.rest.file;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import tfc.biblioteca.domain.file.UploadFileDTO;
import tfc.biblioteca.domain.category.CategoryDTO;

import java.util.ArrayList;
import java.util.List;



@Mapper
public interface UploadFileTOMapper {

    UploadFileTO toTO(UploadFileDTO dto);

    /*@Named("mapCategories")
    public static ArrayList<CategoryDTO> mapCategories(List<Long> categoryIds) {
        ArrayList<CategoryDTO> categories = new ArrayList<>();
        categoryIds.forEach((id) -> {
            CategoryDTO category = new CategoryDTO(id, "");
            categories.add(category);
        });
        return categories;
    }
    @Mapping(source = "categoryIds", target = "categories", qualifiedByName = "mapCategories")*/
    UploadFileDTO toDTO(UploadFileTO to);
    List<UploadFileTO> toTOList(List<UploadFileDTO> list);
    List<UploadFileDTO> toDTOList(List<UploadFileTO> list);
}
