package tfc.biblioteca.domain.image;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ImageService {

    ImageDTO save(ImageDTO dtos);
}
