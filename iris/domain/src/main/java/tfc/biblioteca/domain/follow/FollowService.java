package tfc.biblioteca.domain.follow;

import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.user.UserDTO;

@Service
public interface FollowService {

    UserDTO save(FollowDTO dto);

    FollowDTO delete(FollowDTO dto);
}
