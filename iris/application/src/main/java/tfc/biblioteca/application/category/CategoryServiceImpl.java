package tfc.biblioteca.application.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.category.CategoryDTO;
import tfc.biblioteca.domain.category.CategoryRepository;
import tfc.biblioteca.domain.category.CategoryService;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<CategoryDTO> getAll() {
        return categoryRepository.getAll();
    }
}
