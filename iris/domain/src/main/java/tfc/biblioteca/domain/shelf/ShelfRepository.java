package tfc.biblioteca.domain.shelf;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShelfRepository {

    public List<ShelfDTO> getAllByUserId(Long userId);

    public ShelfDTO findById(Long shelfId);

    public ShelfDTO savePostOnShelf(Long shelfId, Long postId);

    public ShelfDTO create(ShelfDTO shelf);
}
