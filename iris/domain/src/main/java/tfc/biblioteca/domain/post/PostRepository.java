package tfc.biblioteca.domain.post;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository {
    List<PostReadDTO> getAll();
    PostReadDTO getById(Long id);

    List<PostReadDTO> getByUserId(Long id);
    PostDTO savePost(PostDTO post);
}
