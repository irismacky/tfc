package tfc.biblioteca.domain.user;

import org.springframework.stereotype.Repository;
import tfc.biblioteca.domain.login.LoginDTO;

import java.util.Optional;

@Repository
public interface UserRepository {

    LoginDTO getLoginCredentials(String username);
    UserDTO getUserByUsername(String username);

    UserDTO save(UserDTO dto);

    UserDTO update(UserDTO dto);
}
