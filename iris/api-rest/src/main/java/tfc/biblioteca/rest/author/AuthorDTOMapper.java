package tfc.biblioteca.rest.author;

import tfc.biblioteca.domain.author.AuthorDTO;

import java.util.List;

public interface AuthorDTOMapper {

    AuthorTO toTO(AuthorDTO dto);
    AuthorDTO toDTO(AuthorTO to);
    List<AuthorTO> toTOList(List<AuthorDTO> list);
    List<AuthorDTO> toDTOList(List<AuthorTO> list);
}
