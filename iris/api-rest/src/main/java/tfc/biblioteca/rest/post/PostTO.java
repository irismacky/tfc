package tfc.biblioteca.rest.post;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;
import tfc.biblioteca.rest.category.CategoryTO;
import tfc.biblioteca.rest.image.ImageTO;
import tfc.biblioteca.rest.shelf.ShelfTO;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PostTO {
    private Long id;

    private String title;

    private String summary;

    private String content;


    private Long userId;

    private List<ImageTO> images;
    private List<CategoryTO> categories;

    private List<Long> shelfIds;
}
