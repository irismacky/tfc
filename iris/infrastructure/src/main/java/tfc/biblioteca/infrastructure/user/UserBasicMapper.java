package tfc.biblioteca.infrastructure.user;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.user.UserBasicDTO;
import tfc.biblioteca.domain.user.UserDTO;

import java.util.List;

@Mapper
public interface UserBasicMapper {

    UserBasicDTO toDTO(User user);
    User toEntity(UserBasicDTO user);

    List<UserBasicDTO> toListDTO(List<User> list);
    List<User> toEntityList(List<UserBasicDTO> list);

}
