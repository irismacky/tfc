package tfc.biblioteca.infrastructure.shelf;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.shelf.ShelfDTO;

import java.util.List;

@Mapper
public interface ShelfMapper {

    public ShelfDTO toDTO(Shelf dto);
    public Shelf toEntity(ShelfDTO dto);

    public List<ShelfDTO> toDTOList(List<Shelf> list);
    public List<Shelf> toEntityList(List<ShelfDTO> list);
}
