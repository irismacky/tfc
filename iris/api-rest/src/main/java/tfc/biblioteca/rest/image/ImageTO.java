package tfc.biblioteca.rest.image;

import lombok.*;
import tfc.biblioteca.rest.post.PostTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImageTO {

    private Long id;
    private byte[] image;

    private PostTO post;
}
