package tfc.biblioteca.infrastructure.file;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.file.FileDTO;

import java.util.List;

@Mapper
public interface FileMapper {

    File toEntity(FileDTO dto);
    FileDTO toDTO(File file);
    List<File> toEntityList(List<FileDTO> list);

    List <FileDTO> toDTOList(List<File> list);
}
