package tfc.biblioteca.domain.file;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
public interface StorageService {

    public void init();

    public String saveFile(MultipartFile file);
    public File getFileFromPath(String path) throws IOException;

}
