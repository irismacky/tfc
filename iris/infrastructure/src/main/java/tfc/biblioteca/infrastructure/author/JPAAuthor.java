package tfc.biblioteca.infrastructure.author;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPAAuthor extends JpaRepository<Author, Long> {

    Author findByName(String name);
}
