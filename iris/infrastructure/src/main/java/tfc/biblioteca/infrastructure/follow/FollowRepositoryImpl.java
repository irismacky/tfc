package tfc.biblioteca.infrastructure.follow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tfc.biblioteca.domain.follow.FollowDTO;
import tfc.biblioteca.domain.follow.FollowRepository;
import tfc.biblioteca.domain.user.UserDTO;
import tfc.biblioteca.infrastructure.user.JPAUser;
import tfc.biblioteca.infrastructure.user.User;
import tfc.biblioteca.infrastructure.user.UserMapper;

@Repository
public class FollowRepositoryImpl implements FollowRepository {
    @Autowired
    FollowMapper mapper;

    @Autowired
    JPAFollow jpaFollow;

    @Autowired
    UserMapper userMapper;

    @Autowired
    JPAUser jpaUser;
    @Override
    public UserDTO save(FollowDTO follow) {
        Follow entity = mapper.toEntity(follow);
        jpaFollow.save(entity);
        User userReturn = jpaUser.findById(entity.getFollower()).get();
        return userMapper.toDTO(userReturn);
    }

    @Override
    public FollowDTO delete(FollowDTO followDTO) {

        Follow deleteEntity = jpaFollow.findByFollowerAndFollowing(followDTO.getFollower(), followDTO.getFollowing());
        jpaFollow.delete(deleteEntity);
        return mapper.toDTO(deleteEntity);
    }
}
