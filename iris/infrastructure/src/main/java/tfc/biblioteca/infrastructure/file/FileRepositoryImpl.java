package tfc.biblioteca.infrastructure.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tfc.biblioteca.domain.file.FileDTO;
import tfc.biblioteca.domain.file.FileRepository;
import tfc.biblioteca.domain.file.UploadFileDTO;
import tfc.biblioteca.infrastructure.author.JPAAuthor;
import tfc.biblioteca.infrastructure.category.Category;
import tfc.biblioteca.infrastructure.category.JPACategory;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FileRepositoryImpl implements FileRepository {

    @Autowired
    JPAFile jpa;

    @Autowired
    JPACategory jpaCategory;



    @Autowired
    FileMapper mapper;



    @Autowired
    UploadFileMapper uploadMapper;
    @Override
    public List<FileDTO> getAll() {
        return null;
                //mapper.toDTOList(jpa.findAll());
    }

    @Override
    public FileDTO getById(Long id) {
        return null;
        //mapper.toDTO(jpa.findById(id).get());
    }

    @Override
    public FileDTO getByPostId(Long id) {
        File file = jpa.findByPostId(id);
        return mapper.toDTO(file);
    }

    @Override
    public List<FileDTO> getByName(String name) {
        return null;
                //mapper.toDTOList(jpa.findByNameContaining(name));
    }

    @Override
    public List<FileDTO> getListByCategoryId(Long id) {
        Category category = jpaCategory.findById(id).get();
        List<FileDTO> fileDTOS = new ArrayList<>();
        //category.getBooks().forEach((bookCategory -> bookDTOS.add(mapper.toDTO(bookCategory.getBook())) ));
        return fileDTOS;
    }

    @Override
    public UploadFileDTO saveBook(UploadFileDTO dto, String path) {
      File book = uploadMapper.toEntity(dto);
        book.setFilePath(path);
        jpa.save(book);
        return null;
    }
}
