package tfc.biblioteca.rest.follow;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FollowTO {

    private Long id;
    private Long following;
    private Long follower;
}
