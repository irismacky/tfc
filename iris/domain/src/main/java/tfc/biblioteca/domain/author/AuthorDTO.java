package tfc.biblioteca.domain.author;

import lombok.Data;

@Data
public class AuthorDTO {

    private Long id;
    private String name;
    private String bio;
}
