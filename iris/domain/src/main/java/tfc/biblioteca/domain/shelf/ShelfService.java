package tfc.biblioteca.domain.shelf;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ShelfService {

    ShelfDTO save(ShelfDTO dto);
    public List<ShelfDTO> getAllByUserId(Long userId);

    ShelfDTO addPostToShelf(Long shelfId, Long postId);
}
