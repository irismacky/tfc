package tfc.biblioteca.infrastructure.follow;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.follow.FollowDTO;

@Mapper
public interface FollowMapper {

    Follow toEntity(FollowDTO dto);
    FollowDTO toDTO(Follow entity);
}
