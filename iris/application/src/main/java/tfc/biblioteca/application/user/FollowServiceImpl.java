package tfc.biblioteca.application.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.follow.FollowDTO;
import tfc.biblioteca.domain.follow.FollowRepository;
import tfc.biblioteca.domain.follow.FollowService;
import tfc.biblioteca.domain.user.UserDTO;

@Service
public class FollowServiceImpl implements FollowService {
    @Autowired
    FollowRepository repository;
    @Override
    public UserDTO save(FollowDTO dto) {
        return repository.save(dto);
    }

    @Override
    public FollowDTO delete(FollowDTO dto) {
        return repository.delete(dto);
    }
}
