package tfc.biblioteca.rest.file;

import lombok.Data;
import tfc.biblioteca.rest.author.AuthorTO;
import tfc.biblioteca.rest.category.CategoryTO;

import java.util.List;

@Data
public class FileTO {

    private Long id;
    private String name;
    private String description;
    private Integer year;
    private List<CategoryTO> categories;
    private AuthorTO author;
}
