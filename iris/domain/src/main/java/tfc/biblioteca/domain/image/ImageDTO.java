package tfc.biblioteca.domain.image;

import lombok.*;
import tfc.biblioteca.domain.post.PostDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImageDTO {

    private Long id;
    private byte[] image;
    private PostDTO post;
}
