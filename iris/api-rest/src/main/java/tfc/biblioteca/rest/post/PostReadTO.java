package tfc.biblioteca.rest.post;

import lombok.*;
import tfc.biblioteca.domain.image.ImageDTO;
import tfc.biblioteca.rest.category.CategoryTO;
import tfc.biblioteca.rest.image.ImageReadTO;
import tfc.biblioteca.rest.image.ImageTO;
import tfc.biblioteca.rest.user.UserTO;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostReadTO {

    private Long id;

    private String title;

    private String summary;

    private String content;

    private Date date;
    private UserTO user;

    private List<ImageReadTO> images;

    private List<CategoryTO> categories;
}
