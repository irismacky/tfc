package tfc.biblioteca.rest.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import tfc.biblioteca.domain.exceptions.UsernameTakenException;
import tfc.biblioteca.domain.login.LoginService;
import tfc.biblioteca.domain.user.UserService;
import tfc.biblioteca.rest.user.UserDTOMapper;
import tfc.biblioteca.rest.user.UserTO;

@RestController
public class LoginController {

    @Autowired
    LoginService loginService;

    @Autowired
    UserService service;

    @Autowired
    LoginDTOMapper mapper;

    @Autowired
    UserDTOMapper userMapper;

    @PostMapping("/login")
    @CrossOrigin("http://localhost:3000")
    public UserTO getUserInfo(@RequestBody LoginTO userInfo){

        return userMapper.toTO(loginService.getUserInfo(mapper.toDTO(userInfo)));
    }
    @PostMapping("/signup")
    @CrossOrigin("http://localhost:3000")
    public UserTO getUserInfo(@RequestBody UserTO userInfo) throws UsernameTakenException {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String rawPassword = userInfo.getPassword();
        userInfo.setPassword(encoder.encode(userInfo.getPassword()));
        UserTO userReturn = userMapper.toTO(service.save(userMapper.toDTO(userInfo)));
        userReturn.setPassword(rawPassword);
        System.out.println("Return signup  " + userReturn.toString());
        return userReturn;
    }

}
