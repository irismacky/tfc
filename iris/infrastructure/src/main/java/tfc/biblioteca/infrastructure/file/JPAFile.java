package tfc.biblioteca.infrastructure.file;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPAFile
        extends JpaRepository<File, Long>

{
    File findByPostId(Long postId);
}
