package tfc.biblioteca.domain.user;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserBasicDTO {

    private Long id;

    private String username;

    private byte[] img;


}
