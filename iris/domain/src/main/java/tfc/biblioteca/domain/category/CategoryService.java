package tfc.biblioteca.domain.category;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {

    List<CategoryDTO> getAll();
}
