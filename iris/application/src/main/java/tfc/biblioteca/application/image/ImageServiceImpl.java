package tfc.biblioteca.application.image;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.image.ImageDTO;
import tfc.biblioteca.domain.image.ImageRepository;
import tfc.biblioteca.domain.image.ImageService;

import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    ImageRepository repository;
    @Override
    public ImageDTO save(ImageDTO dtos) {
        return repository.save(dtos);
    }
}
