package tfc.biblioteca.infrastructure.shelf;

import jakarta.persistence.*;
import lombok.Data;
import tfc.biblioteca.infrastructure.category.Category;
import tfc.biblioteca.infrastructure.post.Post;
import tfc.biblioteca.infrastructure.user.User;

import java.util.List;

@Entity
@Data
@Table(name = "t_shelf")
public class Shelf {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "shelf_id")
    private Long id;

    @Column(name = "shelf_name")
    private String name;

    @Column(name = "shelf_color")
    private String color;

    @ManyToMany
    @JoinTable(
            name = "t_post_shelf",
            joinColumns = @JoinColumn(name = "shelf_id"),
            inverseJoinColumns = @JoinColumn(name = "post_id"))
    List<Post> posts;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


}
