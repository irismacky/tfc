package tfc.biblioteca.application.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.exceptions.UsernameTakenException;
import tfc.biblioteca.domain.user.UserDTO;
import tfc.biblioteca.domain.user.UserRepository;
import tfc.biblioteca.domain.user.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repository;
    @Override
    public UserDTO save(UserDTO dto) throws UsernameTakenException {
        UserDTO checkUserValid = repository.getUserByUsername(dto.getUsername());
        if (checkUserValid == null){
             repository.save(dto);
        } else {
            throw new UsernameTakenException();
        }


        return getByUsername(dto.getUsername());
    }

    @Override
    public UserDTO getByUsername(String username) {
        return repository.getUserByUsername(username);
    }

    @Override
    public UserDTO patchUpdate(UserDTO dto, String username) {
        UserDTO updateUser = getByUsername(username);
        if (!dto.getUsername().isBlank()){
            updateUser.setUsername(dto.getUsername());
        }

        if (!dto.getDescription().isBlank()){
            updateUser.setDescription(dto.getDescription());
        }

        if (dto.getImg() != null){
            updateUser.setImg(dto.getImg());
        }


        return repository.update(updateUser);
    }

    @Override
    public UserDTO addUserImg(byte[] img, String username) {
        UserDTO user = getByUsername(username);
        user.setImg(img);

        return repository.save(user);
    }
}
