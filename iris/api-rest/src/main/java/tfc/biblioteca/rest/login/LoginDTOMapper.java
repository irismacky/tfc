package tfc.biblioteca.rest.login;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.login.LoginDTO;

@Mapper
public interface LoginDTOMapper {

    LoginDTO toDTO(LoginTO to);
}
