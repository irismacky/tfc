package tfc.biblioteca.infrastructure.category;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JPACategory extends JpaRepository<Category, Long> {

    Optional<Category> findById(Long id);
}
