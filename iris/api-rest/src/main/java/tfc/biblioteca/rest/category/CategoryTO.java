package tfc.biblioteca.rest.category;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CategoryTO {

    private Long id;
    private String name;


}
