package tfc.biblioteca.infrastructure.post;

import org.springframework.data.jpa.repository.JpaRepository;

public interface JPAPost extends JpaRepository<Post, Long> {
}
