package tfc.biblioteca.infrastructure.author;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.author.AuthorDTO;

import java.util.List;

@Mapper
public interface AuthorMapper {

    Author toEntity(AuthorDTO dto);
    AuthorDTO toDTO(Author entity);
    List<Author> toEntityList(List<AuthorDTO> list);
    List <AuthorDTO> toDTOList(List<Author> list);
}
