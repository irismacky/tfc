package tfc.biblioteca.infrastructure.file;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.file.UploadFileDTO;

@Mapper
public interface UploadFileMapper {


   /*@Named(value = "mapCategories")
    public default List<Category> mapCategories(List<CategoryDTO> categories) {
        CategoryRepositoryImpl repository = new CategoryRepositoryImpl();
        List<Category> categoriesArray = new ArrayList<>();
        categories.forEach((categoryDTO -> categoriesArray.add(repository.findById(categoryDTO.getId())) ));

        return categoriesArray;
    }
    @Mapping(source = "categories", target = "categories", qualifiedByName = "mapCategories")*/
    File toEntity(UploadFileDTO dto);
  /*  UploadBookDTO toDTO(Book book);
    List<Book> toEntityList(List<UploadBookDTO> list);
    List <UploadBookDTO> toDTOList(List<Book> list);*/
}
