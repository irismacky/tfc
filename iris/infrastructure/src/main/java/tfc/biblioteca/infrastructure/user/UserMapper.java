package tfc.biblioteca.infrastructure.user;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.user.UserDTO;

@Mapper
public interface UserMapper {
    UserDTO toDTO(User user);
    User toEntity(UserDTO user);
}
