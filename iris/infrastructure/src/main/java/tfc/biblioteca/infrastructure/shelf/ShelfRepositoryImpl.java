package tfc.biblioteca.infrastructure.shelf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tfc.biblioteca.domain.shelf.ShelfDTO;
import tfc.biblioteca.domain.shelf.ShelfRepository;
import tfc.biblioteca.infrastructure.post.JPAPost;
import tfc.biblioteca.infrastructure.post.Post;
import tfc.biblioteca.infrastructure.user.JPAUser;
import tfc.biblioteca.infrastructure.user.User;

import java.util.List;

@Repository
public class ShelfRepositoryImpl implements ShelfRepository {

    @Autowired
    JPAShelf jpaShelf;

    @Autowired
    JPAUser jpaUser;

    @Autowired
    JPAPost jpaPost;

    @Autowired
    ShelfMapper mapper;

    @Override
    public List<ShelfDTO> getAllByUserId(Long userId) {
        User user = jpaUser.findById(userId).get();
        List<Shelf> shelves = jpaShelf.findAllByUser(user);
        return mapper.toDTOList(shelves);
    }

    @Override
    public ShelfDTO findById(Long shelfId) {
        return mapper.toDTO(jpaShelf.findById(shelfId).get());
    }

    @Override
    public ShelfDTO savePostOnShelf(Long shelfId, Long postId) {
        Shelf shelf = jpaShelf.findById(shelfId).get();
        Post post =  jpaPost.findById(postId).get();

        shelf.getPosts().add(post);


        return mapper.toDTO(jpaShelf.save(shelf));
    }

    @Override
    public ShelfDTO create(ShelfDTO shelf) {
        User user = jpaUser.findById(shelf.getUserId()).get();
        Shelf shelfEntity = mapper.toEntity(shelf);
        shelfEntity.setUser(user);


        return mapper.toDTO(jpaShelf.save(shelfEntity));
    }
}
