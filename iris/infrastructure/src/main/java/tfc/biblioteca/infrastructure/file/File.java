package tfc.biblioteca.infrastructure.file;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_file")
public class File {

   @Id
   @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "file_id")
    private Long id;
    @Column(name ="file_path")
    private String filePath;

    @Column(name="post_id")
    private Long postId;

}
