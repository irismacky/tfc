package tfc.biblioteca.rest.user;

import lombok.*;
import tfc.biblioteca.domain.user.UserBasicDTO;
import tfc.biblioteca.rest.shelf.ShelfTO;

import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserTO {



    private Long id;


    private String username;


    private String password;

    private String email;


    private byte[] img;


    private String description;


    private String joinDate;

    private List<UserBasicDTO> followers;
    private List<UserBasicDTO> following;




}
