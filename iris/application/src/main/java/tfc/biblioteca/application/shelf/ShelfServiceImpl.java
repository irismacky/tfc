package tfc.biblioteca.application.shelf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.domain.post.PostReadDTO;
import tfc.biblioteca.domain.post.PostRepository;
import tfc.biblioteca.domain.shelf.ShelfDTO;
import tfc.biblioteca.domain.shelf.ShelfRepository;
import tfc.biblioteca.domain.shelf.ShelfService;

import java.util.List;

@Service
public class ShelfServiceImpl implements ShelfService {

    @Autowired
    ShelfRepository repository;

    @Autowired
    PostRepository postRepository;

    @Override
    public ShelfDTO save(ShelfDTO dto) {
        return repository.create(dto);
    }

    @Override
    public List<ShelfDTO> getAllByUserId(Long userId) {
        return repository.getAllByUserId(userId);
    }

    @Override
    public ShelfDTO addPostToShelf(Long shelfId, Long postId) {

        return repository.savePostOnShelf(shelfId, postId);
    }
}
