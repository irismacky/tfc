package tfc.biblioteca.infrastructure.post;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.web.multipart.MultipartFile;
import tfc.biblioteca.domain.category.CategoryDTO;
import tfc.biblioteca.domain.post.PostDTO;
import tfc.biblioteca.infrastructure.category.Category;
import tfc.biblioteca.infrastructure.image.Image;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface PostMapper {

    /*@Named("byteArrayToImg")
    public static List<Image> multiPartFileToImg(List<MultipartFile> files) {
        List<Image> images = new ArrayList<>();
        return images;
    }

    @Named("imgToByteArray")
    public static List<byte[]> imgToByteArray(List<Image> images) {
        List<byte[]> bytes = new ArrayList<>();
        for(Image img : images){
            bytes.add(img.getImage());
        }
        return bytes;
    }*/

    //@Mapping(source = "images", target = "images", qualifiedByName = "byteArrayToImg")
    Post toEntity(PostDTO dto);


    //@Mapping(source = "images", target = "images", qualifiedByName = "imgToByteArray")
    PostDTO toDto(Post entity);


    //@Mapping(source = "images", target = "images", qualifiedByName = "byteArrayToImg")
    List<Post> toEntityList(List<PostDTO> list);
   // @Mapping(source = "images", target = "images", qualifiedByName = "imgToByteArray")
    List<PostDTO> toDtoList(List<Post> list);
}
