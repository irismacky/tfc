package tfc.biblioteca.domain.post;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;
import tfc.biblioteca.domain.author.AuthorDTO;
import tfc.biblioteca.domain.category.CategoryDTO;
import tfc.biblioteca.domain.image.ImageDTO;
import tfc.biblioteca.domain.user.UserDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {

    private Long id;
    private String title;
    private String summary;
    private String content;
    private Date date;
    private AuthorDTO author;
    private List<ImageDTO> images;
    private List<CategoryDTO> categories;
    private List<Long> shelfIds;
    private Long userId;
}
