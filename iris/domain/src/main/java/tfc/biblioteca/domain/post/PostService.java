package tfc.biblioteca.domain.post;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {

    List<PostReadDTO> getAll();

    PostReadDTO getById(Long id);

    List<PostReadDTO> getByUserId(Long id);

    PostDTO savePost(PostDTO post);
}
