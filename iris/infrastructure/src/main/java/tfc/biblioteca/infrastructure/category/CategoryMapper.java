package tfc.biblioteca.infrastructure.category;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.category.CategoryDTO;

import java.util.List;

@Mapper
public interface CategoryMapper {

    Category toEntity(CategoryDTO dto);


    CategoryDTO toDto(Category entity);

    List<Category> toEntityList(List<CategoryDTO> list);
    List<CategoryDTO> toDtoList(List<Category> list);
}
