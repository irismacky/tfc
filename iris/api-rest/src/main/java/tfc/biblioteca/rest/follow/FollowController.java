package tfc.biblioteca.rest.follow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tfc.biblioteca.domain.follow.FollowDTO;
import tfc.biblioteca.domain.follow.FollowService;
import tfc.biblioteca.domain.user.UserDTO;
import tfc.biblioteca.rest.user.UserDTOMapper;
import tfc.biblioteca.rest.user.UserTO;

@RestController
@CrossOrigin(origins =  "*")
@RequestMapping("/follow")
public class FollowController {

    @Autowired
    FollowService service;

    @Autowired
    FollowDTOMapper mapper;

    @Autowired
    UserDTOMapper userDTOMapper;


    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<UserTO> postFollow(@RequestBody FollowTO follow){
        FollowDTO followDTO = mapper.toDTO(follow);
        UserDTO savedFollow = service.save(followDTO);

        return new ResponseEntity<>(userDTOMapper.toTO(savedFollow), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    ResponseEntity<FollowTO> unfollow(@RequestBody FollowTO follow){
        FollowDTO followDTO = mapper.toDTO(follow);
        FollowDTO unfollow = service.delete(followDTO);

        return new ResponseEntity<>(mapper.toTO(unfollow), HttpStatus.OK);
    }
}
