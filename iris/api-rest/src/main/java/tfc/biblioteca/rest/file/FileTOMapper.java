package tfc.biblioteca.rest.file;

import org.mapstruct.Mapper;
import tfc.biblioteca.domain.file.FileDTO;

import java.util.List;

@Mapper
public interface FileTOMapper {

    FileTO toTO(FileDTO dto);


    FileDTO toDTO(FileTO to);
    List<FileTO> toTOList(List<FileDTO> list);
    List<FileDTO> toDTOList(List<FileTO> list);
}
