package tfc.biblioteca.rest.category;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tfc.biblioteca.domain.category.CategoryService;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/category")
@ComponentScan(basePackages = "tfc.biblioteca.rest")
public class CategoryController {

    @Autowired
    public CategoryDTOMapper categoryMapper;

    @Autowired
    CategoryService categoryService;


    @GetMapping(value = "/all")
     ResponseEntity<List<CategoryTO>> getAllScores() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        return new ResponseEntity<>(categoryMapper.toTOList(categoryService.getAll()), headers, HttpStatus.OK);

    }



}
