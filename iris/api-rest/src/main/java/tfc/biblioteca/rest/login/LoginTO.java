package tfc.biblioteca.rest.login;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginTO {

    private String username;
    private String password;
}
