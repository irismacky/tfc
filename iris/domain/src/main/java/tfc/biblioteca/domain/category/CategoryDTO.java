package tfc.biblioteca.domain.category;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDTO {

    public Long id;
    public String name;
   // private Integer numVolumes;
}
